﻿namespace AdventOfCode
{
    public interface IInstruction
    {
        (long output, long stride) Run(IntComputer intComputer);
    }
}
