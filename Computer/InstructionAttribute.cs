﻿using System;

namespace AdventOfCode
{
    public class InstructionAttribute : Attribute
    {
        public readonly int Opcode;

        public InstructionAttribute(int opcode)
        {
            Opcode = opcode;
        }
    }
}
