﻿namespace AdventOfCode
{
    [Instruction(opcode: 9)]
    public class AdjustRelativeBase : IInstruction
    {
        (long output, long stride) IInstruction.Run(IntComputer intComputer)
        {
            var parameter1 = intComputer.Read(1);

            intComputer.RelativeBase += parameter1;

            return (0, 2);
        }
    }
}
