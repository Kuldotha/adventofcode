﻿namespace AdventOfCode
{
    [Instruction(opcode: 99)]
    public class Halt : IInstruction
    {
        (long output, long stride) IInstruction.Run(IntComputer intComputer)
        {
            return (-1, 0);
        }
    }
}
