﻿namespace AdventOfCode
{
    [Instruction(opcode: 3)]
    public class Input : IInstruction
    {
        (long output, long stride) IInstruction.Run(IntComputer intComputer)
        {
            var input = intComputer.GetInput();
            intComputer.Write(1, input);

            return (0, 2);
        }
    }
}
