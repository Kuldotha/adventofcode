﻿namespace AdventOfCode
{
    [Instruction(opcode: 5)]
    public class JumpIfTrue : IInstruction
    {
        (long output, long stride) IInstruction.Run(IntComputer intComputer)
        {
            var parameter1 = intComputer.Read(1);
            var parameter2 = intComputer.Read(2);

            return (0, parameter1 == 0 ? 3 : parameter2 - intComputer.Pointer);
        }
    }
}
