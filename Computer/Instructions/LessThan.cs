﻿namespace AdventOfCode
{
    [Instruction(opcode: 7)]
    public class LessThan : IInstruction
    {
        (long output, long stride) IInstruction.Run(IntComputer intComputer)
        {
            var parameter1 = intComputer.Read(1);
            var parameter2 = intComputer.Read(2);
            intComputer.Write(3, parameter1 < parameter2 ? 1 : 0);

            return (0, 4);
        }
    }
}
