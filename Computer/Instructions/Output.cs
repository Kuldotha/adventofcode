﻿namespace AdventOfCode
{
    [Instruction(opcode: 4)]
    public class Output : IInstruction
    {
        (long output, long stride) IInstruction.Run(IntComputer intComputer)
        {
            var parameter1 = intComputer.Read(1);

            return (parameter1, 2);
        }
    }
}
