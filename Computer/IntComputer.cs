﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    public class IntComputer
    {
        private Dictionary<int, IInstruction> instructions
            = new Dictionary<int, IInstruction>();

        private long[] rom;
        private long[] ram;
        private Queue<long> input;

        public long Output { get; private set; } = 0;

        public long Pointer { get; private set; }
        public long RelativeBase { get; set; }

        public IntComputer()
        {
            input = new Queue<long>();
        }

        public void LoadProgram(long[] rom)
        {
            this.rom = rom;
            this.ram = new long[rom.Length];
            RebootProgram();
        }

        public void RebootProgram()
        {
            rom.CopyTo(ram, 0L);

            input.Clear();
            ResetPointer();
        }

        public void ResetPointer()
        {
            Pointer = 0;
            RelativeBase = 0;
        }

        public void InsertInput(params long[] input)
        {
            foreach (var value in input)
            {
                this.input.Enqueue(value);
            }
        }

        public long GetInput()
        {
            return input.Dequeue();
        }

        public long Run()
        {
            while (Pointer < ram.Length)
            {
                var opcode = (int)(ram[Pointer] % 100);
                if (!instructions.TryGetValue(opcode, out var instruction))
                {
                    // Find instruction type
                    var types = typeof(IntComputer)
                        .Assembly
                        .GetTypes();

                    var instructionType = types
                        .FirstOrDefault(x =>
                        {
                            var customAttributes = x.GetCustomAttributes(typeof(InstructionAttribute), true);
                            if (customAttributes.Length == 0)
                                return false;

                            if (customAttributes[0] is InstructionAttribute instructionAttribute)
                                return instructionAttribute.Opcode == opcode;

                            return false;
                        });

                    instruction = (IInstruction)Activator.CreateInstance(instructionType);
                    instructions[opcode] = instruction;
                }

                switch (instruction)
                {
                    case Input inputInstruction:
                        if (!input.Any())
                            return 0;

                        break;
                }

                var (output, stride) = instruction.Run(this);
                Pointer += stride;

                switch (instruction)
                {
                    case Halt exitInstruction:
                        return 1;

                    case Output outputInstruction:
                        Output = output;
                        break;

                    default:
                        break;
                }
            }

            return 2;
        }

        public long Read(int parameter)
        {
            var mode = ram[Pointer] / (int)Math.Pow(10, parameter + 1) % 10;
            var pointer = 0L;

            switch (mode)
            {
                case 0:
                    pointer = ram[Pointer + parameter];
                    break;
                case 1:
                    return ram[Pointer + parameter];
                case 2:
                    pointer = RelativeBase + ram[Pointer + parameter];
                    break;
            }

            return pointer < ram.Length ? ram[pointer] : 0;
        }

        public void Write(int parameter, long value)
        {
            var mode = ram[Pointer] / (int)Math.Pow(10, parameter + 1) % 10;
            var pointer = 0L;

            switch (mode)
            {
                case 0:
                case 1:
                    pointer = ram[Pointer + parameter];
                    break;
                case 2:
                    pointer = RelativeBase + ram[Pointer + parameter];
                    break;
            }

            // Extend ram if necessary
            if (ram.Length <= pointer)
            {
                var buffer = new long[pointer + 1];
                ram.CopyTo(buffer, 0);
                ram = buffer;
            }

            ram[pointer] = value;
        }
    }
}
