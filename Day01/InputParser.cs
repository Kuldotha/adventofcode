﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode.Day01
{
    public static class InputParser
    {
        public static IEnumerable<Module> ReadAndParseInput()
        {
            var input = File.ReadAllText("./Day01/input.txt");
            var lines = input.Split('\n');

            var modules = lines.Select(x => new Module(int.Parse(x)));
            return modules;
        }
    }
}
