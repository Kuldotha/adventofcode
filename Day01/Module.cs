﻿namespace AdventOfCode.Day01
{
    public class Module
    {
        public readonly int Mass;

        public Module(int mass)
        {
            Mass = mass;
        }
    }
}
