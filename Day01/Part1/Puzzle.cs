﻿using System;
using System.Linq;

namespace AdventOfCode.Day01.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 3479429;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var modules = InputParser.ReadAndParseInput();
            var totalFuelRequired = modules.Sum(x => CalculateFuelRequirement(x));
            return totalFuelRequired;
        }

        private int CalculateFuelRequirement(Module module)
        {
            return Math.Max(0, module.Mass / 3 - 2);
        }
    }
}
