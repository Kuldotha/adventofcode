﻿using System.Linq;

namespace AdventOfCode.Day01.Part2
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 5216273;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var modules = InputParser.ReadAndParseInput();
            var totalFuelRequired = modules.Sum(x => CalculateFuelRequirement(x.Mass));
            return totalFuelRequired;
        }

        private int CalculateFuelRequirement(int mass)
        {
            var fuelRequirement = mass / 3 - 2;
            if (fuelRequirement <= 0)
                return 0;

            var additionalMass = fuelRequirement;
            var additionalFuel = CalculateFuelRequirement(additionalMass);
            fuelRequirement += additionalFuel;

            return fuelRequirement;
        }
    }
}
