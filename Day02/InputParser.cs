﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode.Day02
{
    public static class InputParser
    {
        public static IEnumerable<int> ReadAndParseInput()
        {
            var input = File.ReadAllText("./Day02/input.txt");
            var lines = input.Split('\n');

            var output = lines.SelectMany(x => x.Split(',')).Select(x => int.Parse(x));
            return output;
        }
    }
}
