﻿using System;
using System.Linq;

namespace AdventOfCode.Day02.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 5866714;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var gravityAssistProgram = InputParser.ReadAndParseInput()
                .ToArray();

            // Return gravity assist program to "1202 program alarm" state
            gravityAssistProgram[1] = 12;
            gravityAssistProgram[2] = 2;

            RunProgram(gravityAssistProgram);

            return gravityAssistProgram[0];
        }

        private void RunProgram(int[] program)
        {
            for (int i = 0; i < program.Length; i += 4)
            {
                var opcode = program[i + 0];
                var input1 = program[i + 1];
                var input2 = program[i + 2];
                var output = program[i + 3];

                switch (opcode)
                {
                    default:
                        continue;
                    case 1:
                        program[output] = program[input1] + program[input2];
                        break;
                    case 2:
                        program[output] = program[input1] * program[input2];
                        break;
                    case 99:
                        return;
                }
            }
        }
    }
}
