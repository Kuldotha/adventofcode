﻿using System;
using System.Linq;

namespace AdventOfCode.Day02.Part2
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 5208;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var input = InputParser.ReadAndParseInput().ToArray();
            var program = new int[input.Length];

            for (int noun = 0; noun < 100; noun++)
            {
                for (int verb = 0; verb < 100; verb++)
                {
                    Array.Copy(input, program, input.Length);
                    var output = RunProgram(program, noun, verb);
                    if (output == 19690720)
                    {
                        return 100 * noun + verb;
                    }
                }
            }

            return -1;
        }

        private int RunProgram(int[] program, int noun, int verb)
        {
            program[1] = noun;
            program[2] = verb;

            for (int i = 0; i < program.Length; i += 4)
            {
                var opcode = program[i + 0];
                if (opcode == 99)
                {
                    return program[0];
                }

                var input1 = program[i + 1];
                var input2 = program[i + 2];
                var output = program[i + 3];

                switch (opcode)
                {
                    default:
                        continue;
                    case 1:
                        program[output] = program[input1] + program[input2];
                        break;
                    case 2:
                        program[output] = program[input1] * program[input2];
                        break;
                }
            }

            return -1;
        }
    }
}
