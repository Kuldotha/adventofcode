﻿using System.IO;

namespace AdventOfCode.Day03
{
    public static class InputParser
    {
        public static (Wire wire1, Wire wire2) ReadAndParseInput()
        {
            var input = File.ReadAllText("./Day03/input.txt");
            var lines = input.Split('\n');

            return (ParseLine(lines[0]), ParseLine(lines[1]));
        }

        private static Wire ParseLine(string line)
        {
            var path = line.Split(',');
            var wire = new Wire();

            for (int i = 0; i < path.Length; i++)
            {
                var direction = path[i][0];
                var distance = int.Parse(path[i].Substring(1));

                switch (direction)
                {
                    case 'R':
                        wire[i] = (distance, IntVector2.right);
                        break;
                    case 'U':
                        wire[i] = (distance, IntVector2.up);
                        break;
                    case 'L':
                        wire[i] = (distance, IntVector2.left);
                        break;
                    case 'D':
                        wire[i] = (distance, IntVector2.down);
                        break;
                }
            }

            return wire;
        }
    }
}
