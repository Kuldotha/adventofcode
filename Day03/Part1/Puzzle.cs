﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Day03.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 258;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var wires = InputParser.ReadAndParseInput();
            var possibleCrossings = new HashSet<IntVector2>();

            // Central port
            var position = IntVector2.zero;
            foreach (var step in wires.wire1)
            {
                for (int i = 0; i < step.distance; i++)
                {
                    position += step.direction;
                    possibleCrossings.Add(position);
                }
            }

            // Reset central port
            position = IntVector2.zero;

            var crossings = new List<IntVector2>();
            foreach (var step in wires.wire2)
            {
                for (int i = 0; i < step.distance; i++)
                {
                    position += step.direction;
                    if (!possibleCrossings.Contains(position))
                        continue;

                    crossings.Add(position);
                }
            }

            var closestCrossing = crossings
                .OrderBy(x => Math.Abs(x.X) + Math.Abs(x.Y))
                .FirstOrDefault();

            return Math.Abs(closestCrossing.X) + Math.Abs(closestCrossing.Y);
        }
    }
}
