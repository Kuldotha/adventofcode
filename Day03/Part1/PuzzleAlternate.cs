﻿using System.Drawing;
using System.Drawing.Imaging;

namespace AdventOfCode.Day03.Part1
{
    public class PuzzleAlternate
    {
        public object Solve()
        {
            var wires = InputParser.ReadAndParseInput();

            var rect = new IntRect();
            var position = IntVector2.zero;
            foreach (var step in wires.wire1)
            {
                position += step.direction * step.distance;
                rect.Encapsulate(position);
            }

            position = IntVector2.zero;
            foreach (var step in wires.wire2)
            {
                position += step.direction * step.distance;
                rect.Encapsulate(position);
            }

            using (Bitmap bitmap = new Bitmap(rect.Width + 1, rect.Height + 1))
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    graphics.Clear(Color.White);
                }

                position = IntVector2.zero;
                foreach (var step in wires.wire1)
                {
                    for (int i = 0; i < step.distance; i++)
                    {
                        position += step.direction;
                        bitmap.SetPixel(position.X - rect.X, position.Y - rect.Y, Color.Blue);
                    }
                }

                position = IntVector2.zero;
                foreach (var step in wires.wire2)
                {
                    for (int i = 0; i < step.distance; i++)
                    {
                        position += step.direction;
                        bitmap.SetPixel(position.X - rect.X, position.Y - rect.Y, Color.Red);
                    }
                }

                bitmap.Save(@".\wires.png", ImageFormat.Png);
            }

            return -1;
        }
    }
}
