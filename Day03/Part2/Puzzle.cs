﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Day03.Part2
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 12304;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var wires = InputParser.ReadAndParseInput();
            var possibleIntersections = new Dictionary<IntVector2, int>();

            // Central port
            var steps = 1;
            var position = IntVector2.zero;
            foreach (var step in wires.wire1)
            {
                for (int i = 0; i < step.distance; i++, steps++)
                {
                    position += step.direction;
                    possibleIntersections[position] = steps;
                }
            }

            // Reset central port
            position = IntVector2.zero;

            steps = 1;
            var intersections = new List<(IntVector2 position, int wire1Steps, int wire2Steps)>();
            foreach (var step in wires.wire2)
            {
                for (int i = 0; i < step.distance; i++, steps++)
                {
                    position += step.direction;
                    if (!possibleIntersections.TryGetValue(position, out var wire1Steps))
                        continue;

                    intersections.Add((position, wire1Steps, steps));
                }
            }

            var closestCrossing = intersections.Min(x => x.wire1Steps + x.wire2Steps);
            return closestCrossing;
        }
    }
}
