﻿using System.Collections;
using System.Collections.Generic;

namespace AdventOfCode.Day03
{
    public class Wire : IEnumerable<(int distance, IntVector2 direction)>
    {
        private List<(int distance, IntVector2 direction)> steps;
        public (int distance, IntVector2 direction) this[int index] 
        {
            get
            {
                return steps[index];
            }
            set
            {
                while (index >= steps.Count)
                {
                    steps.Add(default((int distance, IntVector2 direction)));
                }
                steps[index] = value;
            }
        }

        public Wire()
        {
            steps = new List<(int distance, IntVector2 direction)>();
        }

        public IEnumerator<(int distance, IntVector2 direction)> GetEnumerator()
        {
            return steps.GetEnumerator() as IEnumerator<(int distance, IntVector2 direction)>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return steps.GetEnumerator();
        }
    }
}
