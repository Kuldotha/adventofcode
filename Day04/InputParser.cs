﻿using System.IO;

namespace AdventOfCode.Day04
{
    public static class InputParser
    {
        public static (int min, int max) ReadAndParseInput()
        {
            var input = File.ReadAllText("./Day04/input.txt");
            var lines = input.Split('\n');
            var range = lines[0].Split('-');

            return (int.Parse(range[0]), int.Parse(range[1]));
        }
    }
}
