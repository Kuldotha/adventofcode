﻿namespace AdventOfCode.Day04.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 475;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var range = InputParser.ReadAndParseInput();
            var code = range.min;

            var validCodes = 0;

            checkCode:
            while (code < range.max)
            {
                var valid = false;

                var a = 0;
                var b = code / 100000 % 10;
                for (int i = 1, d = 10000; i < 6; i++, d /= 10)
                {
                    a = b;
                    b = code / d % 10;

                    if (a == b)
                        valid = true;
                    else if (b < a)
                    {
                        // Code was invalid, fix for next iteration and start next iteration
                        for (; d >= 1; d /= 10)
                        {
                            b = code / d % 10;

                            code -= b * d;
                            code += a * d;
                        }

                        goto checkCode;
                    }
                }

                // A code that isn't valid here doesn't have a double number in it
                if (valid)
                {
                    validCodes++;
                }

                code++;
            }

            return validCodes;
        }
    }
}
