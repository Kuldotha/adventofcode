﻿using System.IO;
using System.Linq;

namespace AdventOfCode.Day06
{
    public static class InputParser
    {
        public static (string body1, string body2)[] ReadAndParseInput()
        {
            var input = File.ReadAllText("./Day06/input.txt");
            var orbits = input.Split('\n')
                .Select(x => x.Trim()) // WTF?
                .Select(x => x.Split(')'))
                .Select(x => (body1: x[0], body2: x[1]))
                .ToArray();

            return orbits;
        }
    }
}
