﻿using System.Collections.Generic;

namespace AdventOfCode.Day06.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 251208;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var orbits = InputParser.ReadAndParseInput();

            var orbitMap = new Dictionary<string, string>();
            foreach (var orbit in orbits)
                orbitMap[orbit.body2] = orbit.body1;

            var totalOrbits = 0;
            foreach (var orbit in orbits)
            {
                var body = orbit.body2;
                while (orbitMap.TryGetValue(body, out body))
                    totalOrbits++;
            }

            return totalOrbits;
        }
    }
}
