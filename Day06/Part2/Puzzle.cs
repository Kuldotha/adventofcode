﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Day06.Part2
{
    public class Puzzle : ISolvable, ITestable
    {
        private Dictionary<string, string> orbitMap;

        bool ITestable.Test()
        {
            object expectedValue = 397;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var orbits = InputParser.ReadAndParseInput();

            orbitMap = new Dictionary<string, string>();
            foreach (var orbit in orbits)
                orbitMap[orbit.body2] = orbit.body1;

            var location = "YOU";
            var youPpath = new List<string>();
            while (orbitMap.TryGetValue(location, out location))
                youPpath.Add(location);

            location = "SAN";
            var sanPath = new List<string>();
            while (orbitMap.TryGetValue(location, out location))
            {
                sanPath.Insert(0, location);

                var index = youPpath.IndexOf(location);
                if (index > -1)
                {
                    youPpath.RemoveRange(index, youPpath.Count - index);
                    break;
                }
            }

            var path = new List<string>();
            path.AddRange(youPpath);
            path.AddRange(sanPath);
            return path.Count - 1;
        }
    }
}
