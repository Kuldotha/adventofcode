﻿using System.IO;
using System.Linq;

namespace AdventOfCode.Day07
{
    public static class InputParser
    {
        public static long[] ReadAndParseInput()
        {
            var input = File.ReadAllText("./Day07/input.txt");
            var lines = input.Split('\n');

            var data = lines.SelectMany(x => x.Split(','))
                .Select(x => long.Parse(x))
                .ToArray();

            return data;
        }
    }
}
