﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Day07.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        private List<IntComputer> intComputers;

        bool ITestable.Test()
        {
            object expectedValue = 21860L;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var amplifierSoftware = InputParser.ReadAndParseInput();
            intComputers = new IntComputer[5]
                .Select(x => new IntComputer())
                .ToList();

            intComputers
                .ForEach(x => x.LoadProgram(amplifierSoftware));

            var settings = GetPossibleSettings("01234");
            var bestSetting = (setting: "", output: 0L);
            foreach (var setting in settings)
            {
                intComputers.ForEach(x => x.RebootProgram());
                var output = TestSetting(setting);
                if (output <= bestSetting.output)
                    continue;

                bestSetting.setting = setting;
                bestSetting.output = output;
            }

            return bestSetting.output;
        }

        private IEnumerable<string> GetPossibleSettings(string s)
        {
            if (s.Length == 1)
                return new string[] { s };

            return s.SelectMany(c => GetPossibleSettings(s.Remove(s.IndexOf(c), 1)).Select(p => string.Format($"{c}{p}")));
        }

        private long TestSetting(string settings)
        {
            var output = 0L;
            for (int i = 0; i < settings.Length; i++)
            {
                intComputers[i].InsertInput(settings[i] - '0', output);
                intComputers[i].Run();
                output = intComputers[i].Output;
            }

            return output;
        }
    }
}
