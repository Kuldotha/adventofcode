﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Day08
{
    public class Image : IEnumerable<ImageLayer>
    {
        public readonly int Width;
        public readonly int Height;

        private List<ImageLayer> layers;

        public Image(int width, int height, IEnumerable<int> pixels)
        {
            Width = width;
            Height = height;

            layers = new List<ImageLayer>();
            while (pixels.Any())
            {
                layers.Add(new ImageLayer(pixels.Take(width * height)));
                pixels = pixels.Skip(width * height);
            }
        }

        IEnumerator<ImageLayer> IEnumerable<ImageLayer>.GetEnumerator()
        {
            return layers.GetEnumerator() as IEnumerator<ImageLayer>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return layers.GetEnumerator();
        }
    }
}
