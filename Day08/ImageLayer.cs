﻿using System.Collections;
using System.Collections.Generic;

namespace AdventOfCode.Day08
{
    public class ImageLayer : IEnumerable<int>
    {
        private IEnumerable<int> pixels;

        public ImageLayer(IEnumerable<int> pixels)
        {
            this.pixels = pixels;
        }

        IEnumerator<int> IEnumerable<int>.GetEnumerator()
        {
            return pixels.GetEnumerator() as IEnumerator<int>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return pixels.GetEnumerator();
        }
    }
}
