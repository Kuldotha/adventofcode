﻿using System.IO;
using System.Linq;

namespace AdventOfCode.Day08
{
    public static class InputParser
    {
        public static Image ReadAndParseInput()
        {
            var input = File.ReadAllText("./Day08/input.txt");
            var lines = input.Split('\n');

            var pixels = lines[0].Select(x => x - '0');
            var image = new Image(25, 6, pixels);

            return image;
        }
    }
}
