﻿using System.Linq;

namespace AdventOfCode.Day08.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 1820;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var image = InputParser.ReadAndParseInput();

            var best = (layer: default(ImageLayer), amount: int.MaxValue);
            foreach (var imageLayer in image)
            {
                var amount = imageLayer.Count(x => x == 0);
                if (amount >= best.amount)
                    continue;

                best.layer = imageLayer;
                best.amount = amount;
            }

            var onesInLayer = best.layer.Count(x => x == 1);
            var twosInLayer = best.layer.Count(x => x == 2);

            return onesInLayer * twosInLayer;
        }
    }
}
