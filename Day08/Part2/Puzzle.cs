﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace AdventOfCode.Day08.Part2
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 0;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var image = InputParser.ReadAndParseInput();
            
            using (Bitmap bitmap = new Bitmap(image.Width, image.Height))
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    graphics.Clear(Color.Transparent);
                }

                var drawnPixels = new HashSet<IntVector2>();
                foreach (var layer in image)
                {
                    int i = -1;
                    foreach (var pixel in layer)
                    {
                        i++;

                        var x = i % image.Width;
                        var y = i / image.Width;
                        if (drawnPixels.Contains(new IntVector2(x, y)))
                        {
                            continue;
                        }

                        var color = Color.Transparent;
                        switch (pixel)
                        {
                            case 0: // Black
                                color = Color.Black;
                                break;
                            case 1: // White
                                color = Color.White;
                                break;
                            case 2: // Transparent
                                continue;
                        }

                        bitmap.SetPixel(x, y, color);
                        drawnPixels.Add(new IntVector2(x, y));
                    }
                }

                bitmap.Save(@".\day08-message.png", ImageFormat.Png);
            }

            return 0;
        }
    }
}
