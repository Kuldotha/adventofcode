﻿namespace AdventOfCode.Day09.Part1
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 3989758265L;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var software = InputParser.ReadAndParseInput();

            var intComputer = new IntComputer();
            intComputer.LoadProgram(software);
            intComputer.InsertInput(1);
            intComputer.Run();
            return intComputer.Output;
        }
    }
}
