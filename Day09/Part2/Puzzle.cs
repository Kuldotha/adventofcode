﻿namespace AdventOfCode.Day09.Part2
{
    public class Puzzle : ISolvable, ITestable
    {
        bool ITestable.Test()
        {
            object expectedValue = 76791L;
            return ((ISolvable)this).Solve().Equals(expectedValue);
        }

        object ISolvable.Solve()
        {
            var software = InputParser.ReadAndParseInput();

            var intComputer = new IntComputer();
            intComputer.LoadProgram(software);
            intComputer.InsertInput(2);
            intComputer.Run();
            return intComputer.Output;
        }
    }
}
