﻿public interface ISolvable
{
    object Solve();
}