﻿public interface ITestable
{
    bool Test();
}