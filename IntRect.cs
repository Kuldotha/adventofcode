﻿using System;

namespace AdventOfCode
{
    public struct IntRect
    {
        private int? xMin;
        private int? yMin;

        private int? xMax;
        private int? yMax;

        public int X
        {
            get { return xMin ?? 0; }
            set { xMin = value; }
        }

        public int Y
        {
            get { return yMin ?? 0; }
            set { yMin = value; }
        }

        public int Width
        {
            get { return (xMax ?? (xMin ?? 0)) - (xMin ?? 0); }
        }

        public int Height
        {
            get { return (yMax ?? (yMin ?? 0)) - (yMin ?? 0); }
        }

        public IntRect(int x, int y, int width, int height)
        {
            xMin = x;
            yMin = y;

            xMax = xMin + width;
            yMax = yMin + height;
        }

        public void Encapsulate(IntVector2 position)
        {
            xMin = Math.Min(xMin ?? position.X, position.X);
            yMin = Math.Min(yMin ?? position.Y, position.Y);

            xMax = Math.Max(xMax ?? position.X, position.X);
            yMax = Math.Max(yMax ?? position.Y, position.Y);
        }
    }
}
