﻿namespace AdventOfCode
{
    public struct IntVector2
    {
        public static IntVector2 zero => new IntVector2(0, 0);

        public static IntVector2 right => new IntVector2(1, 0);
        public static IntVector2 up => new IntVector2(0, 1);
        public static IntVector2 left => new IntVector2(-1, 0);
        public static IntVector2 down => new IntVector2(0, -1);

        public int X;
        public int Y;

        public IntVector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is IntVector2 vector))
                return false;

            return this == vector;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"[{X},{Y}]";
        }

        public static bool operator ==(IntVector2 a, IntVector2 b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(IntVector2 a, IntVector2 b)
        {
            return !(a == b);
        }

        public static IntVector2 operator +(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.X + b.X, a.Y + b.Y);
        }

        public static IntVector2 operator -(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.X - b.X, a.Y - b.Y);
        }

        public static IntVector2 operator *(IntVector2 v, int f)
        {
            return new IntVector2(v.X * f, v.Y * f);
        }
    }
}
