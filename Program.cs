﻿using System;
using System.Diagnostics;
using System.Linq;

namespace AdventOfCode
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = "";
            while (true)
            {
                input = Console.ReadLine();

                var elements = input.Split(' ');
                if (elements.Length == 0)
                    continue;

                var command = elements[0];
                switch (command)
                {
                    case "q":
                    case "quit":
                    case "exit":
                        return;

                    case "test":
                        {
                            if (elements.Length == 1 || !int.TryParse(elements[1], out var day))
                            {
                                RunTests();
                                continue;
                            }

                            if (!int.TryParse(elements[2], out var part))
                            {
                                continue;
                            }

                            RunTest(day.ToString("D2"), part.ToString("D1"));
                        }
                        break;

                    case "run":
                        {
                            if (!int.TryParse(elements[1], out var day))
                            {
                                continue;
                            }

                            if (!int.TryParse(elements[2], out var part))
                            {
                                continue;
                            }

                            RunPuzzle(day.ToString("D2"), part.ToString("D1"));
                        }
                        break;
                }
            }
        }

        public static void RunPuzzle(string day, string part)
        {
            var stopwatch = Stopwatch.StartNew();

            var puzzleType = Type.GetType($"AdventOfCode.Day{day}.Part{part}.Puzzle, AdventOfCode, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
            var puzzle = Activator.CreateInstance(puzzleType);

            Console.WriteLine(((ISolvable)puzzle).Solve());
            Console.WriteLine($"Elapsed time: {stopwatch.ElapsedMilliseconds}ms");
        }

        private static void RunTest(string day, string part)
        {
            RunTest($"AdventOfCode.Day{day}.Part{part}.Puzzle, AdventOfCode, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
        }

        private static void RunTest(string puzzleType)
        {
            var testableType = Type.GetType(puzzleType);
            if (!(Activator.CreateInstance(testableType) is ITestable testable))
                return;

            var day = testable.ToString().Split('.')[1];
            var part = testable.ToString().Split('.')[2];
            Console.Write($"{day} {part} ");

            var succes = testable.Test();
            Console.BackgroundColor = succes ? ConsoleColor.Green : ConsoleColor.Red;
            Console.ForegroundColor = succes ? ConsoleColor.DarkGreen : ConsoleColor.DarkRed;
            Console.Write(succes ? "succeeded" : "failed");
            Console.WriteLine();
            Console.ResetColor();
        }

        private static void RunTests()
        {
            var puzzles = typeof(Program).Assembly.GetTypes()
                .Where(x => x.GetInterface("ITestable") != null)
                .OrderBy(x => x.AssemblyQualifiedName)
                .Select(x => x.AssemblyQualifiedName);

            // Test all previous puzzles
            foreach (var puzzleType in puzzles)
                RunTest(puzzleType);
        }
    }
}
